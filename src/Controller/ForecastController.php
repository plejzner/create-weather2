<?php

namespace App\Controller;

use App\Entity\City;
use App\Form\Type\CityType;
use App\Form\Type\SearchCityType;
use App\Service\ForecastHistoryService;
use App\Service\ForecastService;
use Doctrine\DBAL\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ForecastController extends AbstractController
{
    #[Route('/', name: 'search_city')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(SearchCityType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('choose_city', $form->getData());
        }

        return $this->render('forecast/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/choose-city/{countrySymbol}/{cityName}', name: 'choose_city')]
    public function chooseCity(Request $request, string $countrySymbol, string $cityName): Response
    {
        $form = $this->createForm(
            CityType::class,
            null,
            ['countrySymbol' => $countrySymbol, 'cityName' => $cityName]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $city = $form->getData()['city'];
            return $this->redirectToRoute('show_weather', ['id' => $city->getId()]);
        }

        return $this->render('forecast/choose_city.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/weather/{id}', name: 'show_weather')]
    public function showWeather(
        City $city,
        ForecastService $forecastService,
        ForecastHistoryService $historyService,
        LoggerInterface $logger
    ): Response
    {
        $avgTemperature = $forecastService->getAvgTemperature($city);

        try {
            $historyService->saveForecast($city, $avgTemperature);
        } catch (Exception $doctrineDbalException) {
            // log saving error, and show result to user anyway
            $logger->error($doctrineDbalException);
        }

        return $this->render('forecast/show_weather.html.twig', [
            'city' => $city,
            'avgTemperature' => $avgTemperature,
        ]);
    }
}
