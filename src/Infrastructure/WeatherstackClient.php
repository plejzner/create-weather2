<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\WeatherApiClient;
use App\ValueObject\Temperature;

class WeatherstackClient implements WeatherApiClient
{
    private const CURRENT_TEMP_URL = 'http://api.weatherstack.com/current';

    public function __construct(
        private string $apiKey,
        private HttpApiClient $httpApiClient
    ) {}

    public function getCurrentTemp(float $latitude, float $longitude): Temperature
    {
        $content = $this->httpApiClient->makeRequest(
            self::CURRENT_TEMP_URL,
            [
                'query' => "{$latitude}, {$longitude}",
                'access_key' => $this->apiKey,
                'units' => 'm'
            ]
        );

        $this->handleErrorsInResponseContent($content);

        return new Temperature($content['current']['temperature'], Temperature::CELSIUS_UNIT);
    }

    private function handleErrorsInResponseContent($content): void
    {
        $errorMessage = '';
        if (isset($content['error']['type'])) {
            $errorMessage .= $content['error']['type'];
        }
        if (isset($content['error']['info'])) {
            $errorMessage .= ' ' . $content['error']['info'];
        }
        if (isset($content['error']['code'])) {
            $errorMessage .= ' ' . $content['error']['code'];
        }
        if (isset($content['success']) && $content['success'] === false) {
            throw new ApiClientException($errorMessage);
        }
        if (isset($content['current']['temperature']) === false ) {
            throw new ApiClientException('malformed response');
        }
    }
}
