<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\WeatherApiClient;
use App\ValueObject\Temperature;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OpenweathermapClient implements WeatherApiClient
{
    private const CURRENT_TEMP_URL = 'https://api.openweathermap.org/data/2.5/weather';

    public function __construct(
        private string $apiKey,
        private HttpClientInterface $httpClient,
    ) {}

    public function getCurrentTemp(float $latitude, float $longitude): Temperature
    {
        try{
            $response = $this->httpClient->request('GET', self::CURRENT_TEMP_URL, ['query' => [
                'lat' => $latitude,
                'lon' => $longitude,
                'appid' => $this->apiKey,
                'units' => 'metric'
            ]]);
            $content = json_decode($response->getContent(), true);

        } catch (ExceptionInterface $httpClientException) {
            throw new ApiClientException('request error', 0, $httpClientException);
        }

        $this->handleErrorsInResponseContent($content);

        return new Temperature($content['main']['temp'], Temperature::CELSIUS_UNIT);
    }

    private function handleErrorsInResponseContent($content): void
    {
        if (isset($content['main']['temp']) === false ) {
            throw new ApiClientException('malformed response');
        }
    }
}
