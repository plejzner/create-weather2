<?php

declare(strict_types=1);

namespace App\Infrastructure;

class ApiClientException extends \Exception
{
}
