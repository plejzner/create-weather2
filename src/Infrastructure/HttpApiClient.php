<?php

declare(strict_types=1);

namespace App\Infrastructure;

use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HttpApiClient
{
    public function __construct(
        private HttpClientInterface $httpClient
    ) {}

    public function makeRequest(string $url, array $params): array
    {
        try {
            $response = $this->httpClient->request('GET', $url, ['query' => $params]);
            $content = json_decode($response->getContent(), true);
        } catch (ExceptionInterface $httpClientException) {
            throw new ApiClientException('request error', 0, $httpClientException);
        }

        return $content;
    }
}
