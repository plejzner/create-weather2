<?php

namespace App\Repository;

use App\Entity\ForecastHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ForecastHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForecastHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForecastHistory[]    findAll()
 * @method ForecastHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForecastHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForecastHistory::class);
    }

    // /**
    //  * @return ForecastHistory[] Returns an array of ForecastHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ForecastHistory
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
