<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\City;
use App\Repository\CityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CityType extends AbstractType
{
    private CityRepository $cityRepository;

    public function __construct(CityRepository $entityRepository)
    {
        $this->cityRepository = $entityRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $qb = $this->cityRepository->createQueryBuilder('c')
            ->where('c.country = ?1')
            ->andWhere('c.name LIKE ?2')
            ->orderBy('c.name', 'ASC')
            ->setParameter(1, $options['countrySymbol'])
            ->setParameter(2, '%'.$options['cityName'].'%');

        $builder->add('city', EntityType::class, [
            'class' => City::class,
            'query_builder' => $qb,
            'choice_label' => 'name'
            ]);

        $builder->add('Choose', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'countrySymbol' => null,
            'cityName' => null
        ]);
    }
}