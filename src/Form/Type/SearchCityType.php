<?php

declare(strict_types=1);

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class SearchCityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('countrySymbol', CountryType::class, ['label' => 'Choose country: '])
            ->add('cityName', TextType::class, [
                'label' => 'City name (or part): ',
                'required' => true,
                'constraints' => new Length(['min' => 2])])
            ->add('Submit', SubmitType::class);
    }
}