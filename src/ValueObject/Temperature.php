<?php

declare(strict_types=1);

namespace App\ValueObject;

use InvalidArgumentException;

class Temperature
{
    const CELSIUS_UNIT = 'C';
    const FAHRENHEIT_UNIT = 'F';
    const VALID_UNITS = [self::CELSIUS_UNIT, self::FAHRENHEIT_UNIT];

    public function __construct(
        private float $value,
        private string $unit
    ) {
        if (!in_array($unit, self::VALID_UNITS)) {
            throw new InvalidArgumentException('invalid unit');
        }
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getUnit(): string
    {
        return $this->unit;
    }

    public function __toString(): string
    {
        return round($this->value, 2) . ' ' . $this->unit;
    }
}
