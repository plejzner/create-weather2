<?php

declare(strict_types=1);

namespace App\Service;

use App\Domain\WeatherApiClient;
use App\Entity\City;
use App\Infrastructure\ApiClientException;
use App\ValueObject\Temperature;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\CacheInterface;

class ForecastService
{
    /** @var WeatherApiClient[] */
    private array $weatherApiClients = [];

    public function __construct(
        private CacheInterface $cache,
        private LoggerInterface $logger
    ) {}

    public function addApiClient(WeatherApiClient $weatherApiClient): void
    {
        $this->weatherApiClients[] = $weatherApiClient;
    }

    public function getAvgTemperature(City $city): Temperature
    {
        if (empty($this->weatherApiClients)) {
            throw new \LogicException('No api clients provided');
        }

        $tempSum = 0;
        $clientsAnswered = 0;
        foreach ($this->weatherApiClients as $apiClient) {
            try {
                // caching per api result. Other option would be to cache whole calculated average temperature.
                $cacheKey = md5($apiClient::class . $city->getId());
                $temp = $this->cache->get($cacheKey, function() use ($apiClient, $city) {
                    return $apiClient->getCurrentTemp($city->getLatitude(), $city->getLongitude());
                });

                $tempSum += $temp->getValue();
                $clientsAnswered++;

            } catch (ApiClientException $exception) {
                // log api failure and proceed with other apis
                $this->logger->error($exception);
            }
        }

        if ($clientsAnswered === 0) {
            throw new \LogicException('All apis failed');
        }

        return new Temperature($tempSum / $clientsAnswered, Temperature::CELSIUS_UNIT);
    }
}
