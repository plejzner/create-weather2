<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\City;
use App\Entity\ForecastHistory;
use App\ValueObject\Temperature;
use Doctrine\ORM\EntityManagerInterface;

class ForecastHistoryService
{
    public function __construct(
        private EntityManagerInterface $em
    ) {}

    public function saveForecast(City $city, Temperature $temperature): void
    {
        $forecastHistory = (new ForecastHistory())
            ->setCity($city)
            ->setDate(new \DateTime('now'))
            ->setTemperature($temperature->getValue())
            ->setTemperatureUnit($temperature->getUnit());
        $this->em->persist($forecastHistory);
        $this->em->flush();
    }
}
