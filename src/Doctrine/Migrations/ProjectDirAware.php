<?php

namespace App\Doctrine\Migrations;

interface ProjectDirAware
{
    public function setProjectDir(string $projectDir): void;
}
