<?php

declare(strict_types=1);

namespace App\Doctrine\Migrations;

use Doctrine\DBAL\Connection;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;

class MigrationFactory implements \Doctrine\Migrations\Version\MigrationFactory
{
    private Connection $connection;
    private LoggerInterface $logger;

    private string $projectDir;

    public function __construct(Connection $connection, LoggerInterface $logger, string $projectDir)
    {
        $this->connection = $connection;
        $this->logger     = $logger;
        $this->projectDir = $projectDir;
    }

    public function createVersion(string $migrationClassName) : AbstractMigration
    {
        $migration = new $migrationClassName(
            $this->connection,
            $this->logger
        );

        if ($migration instanceof ProjectDirAware) {
            $migration->setProjectDir($this->projectDir);
        }

        return $migration;
    }
}
