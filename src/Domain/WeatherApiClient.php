<?php

declare(strict_types=1);

namespace App\Domain;

use App\Infrastructure\ApiClientException;
use App\ValueObject\Temperature;

interface WeatherApiClient
{
    /** @throws ApiClientException */
    public function getCurrentTemp(float $latitude, float $longitude): Temperature;
}
