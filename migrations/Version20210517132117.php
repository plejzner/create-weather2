<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Doctrine\Migrations\ProjectDirAware;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210517132117 extends AbstractMigration implements ProjectDirAware
{
    const CITY_LIST_FILE_PATH = '/var/city.list.json';

    private string $projectDir;

    public function getDescription(): string
    {
        return 'Imports cities from file';
    }

    public function up(Schema $schema): void
    {
        $cities = json_decode(file_get_contents($this->projectDir . self::CITY_LIST_FILE_PATH),true);

        foreach ($cities as $city) {
            $sql = "insert into city(weathermap_id, name, state, country, latitude, longitude) VALUES (:weathermap_id, :name, :state, :country, :latitude, :longitude)";
            $this->addSql($sql, [
                $city['id'],
                $city['name'],
                $city['state'],
                $city['country'],
                $city['coord']['lat'],
                $city['coord']['lon']
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('delete from city');
    }

    public function setProjectDir(string $projectDir): void
    {
        $this->projectDir = $projectDir;
    }
}
