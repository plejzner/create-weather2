<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210517131608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creates city table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE city (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, weathermap_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, state VARCHAR(255) DEFAULT NULL, country VARCHAR(2) NOT NULL, latitude DOUBLE PRECISION NOT NULL, longitude DOUBLE PRECISION NOT NULL)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE city');
    }
}
